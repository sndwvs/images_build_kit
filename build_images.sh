#!/bin/bash



if [ -z $CWD ]; then
    exit
fi

get_name_rootfs() {
    # name for rootfs image
    image_type="$1"
    KERNEL_VERSION=$(get_version $SOURCE/$KERNEL_DIR)

    if [[ $image_type == core ]]; then
        ROOTFS_CORE="${ROOTFS_NAME}-${ARCH}-${image_type}-$BOARD_NAME-$KERNEL_VERSION-build-${ROOTFS_VERSION}"
    fi
    ROOTFS="${ROOTFS_NAME}-${ARCH}-${image_type}-$BOARD_NAME-$KERNEL_VERSION-build-${ROOTFS_VERSION}"
}


clean_rootfs() {
    image_type="$1"

    if [[ ! -z $ROOTFS ]] && [[ -d $SOURCE/$ROOTFS ]]; then
        message "" "clean" "$ROOTFS"
        rm -rf $SOURCE/$ROOTFS >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}


prepare_rootfs() {
    image_type="$1"

    get_name_rootfs $image_type
    clean_rootfs $image_type

    message "" "prepare" "$ROOTFS"
    mkdir -p $SOURCE/$ROOTFS >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


setting_fstab() {
    if [[ ! $(grep ${UUID_ROOT_FS_EXT4} "$SOURCE/$ROOTFS/etc/fstab") ]]; then
        message "" "setting" "fstab"
        sed -i "s:# tmpfs:tmpfs:" $SOURCE/$ROOTFS/etc/fstab
        if [[ $DISTR == sla* ]]; then
            echo "UUID=${UUID_ROOT_FS_EXT4}    /          ext4    noatime,nodiratime,data=writeback,errors=remount-ro       0       1" >> $SOURCE/$ROOTFS/etc/fstab || exit 1
            if [[ $SOCFAMILY == sun20iw1p1 && $KERNEL_SOURCE != next ]]; then
                echo "debugfs    /sys/kernel/debug      debugfs  defaults       0       0" >> $SOURCE/$ROOTFS/etc/fstab
            elif [[ $(echo ${DISK_PARTITIONS[0]} | grep -E '.*boot.*fat.*') ]]; then
                echo "UUID=${UUID_BOOT_FS_VFAT}    /boot      vfat    defaults       0       1" >> $SOURCE/$ROOTFS/etc/fstab
            elif [[ $(echo ${DISK_PARTITIONS[*]} | grep -Po '.*\d+:boot:ext4.*') ]]; then
                echo "UUID=${UUID_BOOT_FS_EXT4}    /boot      ext4    noatime,nodiratime       0       1" >> $SOURCE/$ROOTFS/etc/fstab
            fi
        elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
            sed -i 's:#\(shm\):\1:' $SOURCE/$ROOTFS/etc/fstab || exit 1
            sed -i "/\# End of file/ i UUID=${UUID_ROOT_FS_EXT4}    \/          ext4    noatime,nodiratime,data=writeback,errors=remount-ro       0       1\n" $SOURCE/$ROOTFS/etc/fstab || exit 1
            if [[ $(echo ${DISK_PARTITIONS[0]} | grep -E '.*boot.*fat.*') ]]; then
                sed -i "/\# End of file/ i UUID=${UUID_BOOT_FS_VFAT}    \/boot      vfat    defaults       0       1\n" $SOURCE/$ROOTFS/etc/fstab
            elif [[ $(echo ${DISK_PARTITIONS[*]} | grep -Po '.*\d+:boot:ext4.*') ]]; then
                sed -i "/\# End of file/ i UUID=${UUID_BOOT_FS_EXT4}    \/boot      ext4    noatime,nodiratime       0       1\n" $SOURCE/$ROOTFS/etc/fstab
            fi
        fi
    fi
}


setting_debug() {
    message "" "setting" "uart debugging"
    if [[ $DISTR == sla* ]]; then
        sed -e 's/#\(ttyS[0-2]\)/\1/' \
            -e '/#ttyS3/{n;/^#/i ttyFIQ0\nttyAMA0\nttyAML0
                 }' \
            -i "$SOURCE/$ROOTFS/etc/securetty"

        sed -e "s/^#\(\(s\(1\)\).*\)\(ttyS0\).*\(9600\)/\1$SERIAL_CONSOLE $SERIAL_CONSOLE_SPEED/" \
            -i "$SOURCE/$ROOTFS/etc/inittab"
    elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
        sed -e '/ttyS0/{n;/^/i ttyS1\nttyS2\nttyS3\nttyFIQ0\nttyAMA0\nttyAML0
                 }' \
            -i "$SOURCE/$ROOTFS/etc/securetty"

        sed -e "s/^#\(\(s\(1\)\).*\).*\(38400\).*\(ttyS0\)\(.*$\)/\1$SERIAL_CONSOLE_SPEED $SERIAL_CONSOLE\6/" \
            -i "$SOURCE/$ROOTFS/etc/inittab"
    fi
}


setting_motd() {
    message "" "setting" "motd message"
    # http://patorjk.com/ font: rectangles
    [[ -f "$CWD/config/environment/motd.${DISTR}" ]] && install -m644 -D "$CWD/config/environment/motd.${DISTR}" "$SOURCE/$ROOTFS/etc/motd"
    [[ -f "$CWD/config/boards/$BOARD_NAME/motd.board" ]] && cat "$CWD/config/boards/$BOARD_NAME/motd.board" >> "$SOURCE/$ROOTFS/etc/motd"
    return 0
}


setting_wifi() {
    message "" "setting" "wifi"
    # add nl80211 wifi driver
    sed -i "s#wext#nl80211,wext#g" $SOURCE/$ROOTFS/etc/rc.d/rc.inet1.conf >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


build_img() {
    local IMAGE="$1"

    [[ -z "$IMAGE" ]] && exit 1

    message "" "build" "image: $IMAGE"

    if [[ $DISK_SECTION_TABLE == dos ]];then
        TABLE="o"
    elif [[ $DISK_SECTION_TABLE == gpt ]];then
        TABLE="g"
    fi

    losetup -D
    LOOP=$(losetup -f)

    losetup $LOOP $SOURCE/$IMAGE.img || exit 1

    # write bootloader for all
    [[ ${SOCFAMILY} != k1 ]] && write_uboot $LOOP

    # create disk table
    message "" "create" "disk table: ${DISK_SECTION_TABLE}"
    echo -e "\n${TABLE}\nw" | fdisk -w auto $LOOP >> $LOG 2>&1 || true

    partprobe $LOOP >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    # device is busy
    sleep 2

    for p in ${DISK_PARTITIONS[*]}; do
        DISK=($(echo $p | tr ":" "\n"))
        PART=${DISK[0]}
        LABEL=$([ ${DISK[1]} != "empty" ] && echo ${DISK[1]} || echo "")
        FS=$([ ${DISK[2]} != "empty" ] && echo ${DISK[2]} || echo "")
        OFFSET=$([ ${DISK[3]} != "empty" ] && echo ${DISK[3]} || echo "")
        SIZE=$([ ${DISK[4]} != "empty" ] && echo ${DISK[4]} || echo "")
        _TYPE=$([ ${DISK[5]} != "empty" ] && echo ${DISK[5]} || echo "")
        if [[ $DISK_SECTION_TABLE == gpt ]]; then
            if [[ ${_TYPE} == EFI ]]; then
                TYPE='EFI System'
            elif [[ ${_TYPE} == BBL ]]; then
                TYPE='HiFive BBL'
            elif [[ ${_TYPE} == FSBL ]]; then
                TYPE='HiFive FSBL'
            fi
        elif [[ $DISK_SECTION_TABLE == dos && ${_TYPE} == EFI ]]; then
            TYPE='EFI (FAT-12/16/32)'
        fi
        message "" "create" "disk partition: part: ${PART} offset: ${OFFSET} size: ${SIZE}"
        [[ $DISK_SECTION_TABLE == dos ]] && PREFIX_FOR_TABLE="\n"
        echo -e "n${PREFIX_FOR_TABLE}\n${PART}\n${OFFSET}\n${SIZE}\nw" | fdisk $LOOP >> $LOG 2>&1 || true
        unset PREFIX_FOR_TABLE
        partprobe $LOOP >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        # device is busy
        sleep 2
        if [[ ! -z ${_TYPE} ]];then
            message "" "set" "disk partition type: part: ${PART} type: ${TYPE} (${_TYPE})"
            [[ $(partx -rgo NR -n -1:-1 $LOOP) > 1 ]] && PREFIX_FOR_TYPE="\n${PART}"
            echo -e "\nt${PREFIX_FOR_TYPE}\n${TYPE}\nw" | fdisk $LOOP >> $LOG 2>&1 || true
            unset PREFIX_FOR_TYPE
            partprobe $LOOP >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            # device is busy
            sleep 2
        fi
        message "" "create" "filesystem: part: ${PART} type: ${FS} label: ${LABEL}"
        if [[ ${FS} == fat ]];then
            mkfs.vfat ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            message "" "set" "filesystem: part: ${PART} label: ${LABEL} uuid: ${UUID_BOOT_FS_VFAT/-/}"
            # set UUID
            mkfs.vfat -i ${UUID_BOOT_FS_VFAT/-/} ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        fi
        if [[ ${FS} == ext4 ]];then
            mkfs.ext4 -F -m 0 -L ${LABEL} ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            # set UUID
            if [[ ${LABEL} =~ boot ]]; then
                UUID_FS=${UUID_BOOT_FS_EXT4}
            else
                UUID_FS=${UUID_ROOT_FS_EXT4}
            fi
            message "" "set" "filesystem: part: ${PART} label: ${LABEL} uuid: ${UUID_FS}"
            echo y | tune2fs -U ${UUID_FS} ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            if [[ ${LABEL} == linuxroot ]]; then
                message "" "tune" "filesystem: part: ${PART} type: ${FS}"
                tune2fs -o journal_data_writeback ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
                tune2fs -O ^has_journal ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
                e2fsck -yf ${LOOP}p${PART} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            fi
        fi
        unset TYPE
    done

    for p in $( echo ${DISK_PARTITIONS[*]} | rev ); do
        DISK=($(echo $p | rev | tr ":" "\n"))
        PART=${DISK[0]}
        LABEL=$([ ${DISK[1]} != "empty" ] && echo ${DISK[1]} || echo "")
        FS=$([ ${DISK[2]} != "empty" ] && echo ${DISK[2]} || echo "")
        if [[ ! -z ${FS} ]]; then
            [[ ${LABEL} =~ boot ]] && MOUNT_BOOT="/boot"
            message "" "create" "mount point: \"image${MOUNT_BOOT}\""
            mkdir -p $SOURCE/image${MOUNT_BOOT}
            message "" "mount" "disk: ${LOOP}p${PART} label: ${LABEL}"
            mount ${LOOP}p${PART} $SOURCE/image${MOUNT_BOOT}
            unset MOUNT_BOOT
        fi
    done

    # write bootloader for: banana_pi_f3, lichee_pi_3a, milk_v_jupiter
    [[ ${SOCFAMILY} == k1 ]] && write_uboot $LOOP

    message "" "copy" "data to image"
    rsync -axHAWXS --numeric-ids "$SOURCE/$IMAGE/" "$SOURCE/image/"

    for p in ${DISK_PARTITIONS[*]}; do
        DISK=($(echo $p | tr ":" "\n"))
        PART=${DISK[0]}
        LABEL=$([ ${DISK[1]} != "empty" ] && echo ${DISK[1]} || echo "")
        FS=$([ ${DISK[2]} != "empty" ] && echo ${DISK[2]} || echo "")
        if [[ ! -z ${FS} ]]; then
            [[ ${LABEL} =~ boot ]] && MOUNT_BOOT="/boot"
            message "" "umount" "disk: ${LOOP}p${PART} label: ${LABEL}"
            umount $SOURCE/image${MOUNT_BOOT}
            unset MOUNT_BOOT
        fi
    done

    if [[ -d $SOURCE/image ]]; then
        rm -rf $SOURCE/image
    fi

    losetup -d $LOOP

    if [[ -f $SOURCE/$IMAGE.img ]]; then
        mv $SOURCE/$IMAGE.img $BUILD/$OUTPUT/$IMAGES
    fi

    message "" "done" "build image"
}


download_pkg() {
    # get parameters
    local url=$1
    local type=$2
    local packages

    # read packages type
    read_packages "${type}" packages

    for pkg in ${packages}; do
        category=$(echo $pkg | cut -f1 -d "/")
        pkg=$(echo $pkg | cut -f2 -d "/")
        if [[ ! -z ${pkg} ]];then

            PKG_NAME=($(lftp -e "set ssl:verify-certificate no; open ${url}/${category}/; cls -B -q; exit;" 2>/dev/null | egrep -o "(^$(echo $pkg | sed 's/+/\\\+/g'))-.*(t.z)" | sort -ur))

            [[ $DISTR == crux* || $DISTR == irradium ]] && PKG_NAME=($(lftp -e "set ssl:verify-certificate no; open ${url}/${category}/; cls -B -q; exit;" 2>/dev/null | egrep -o "(^$(echo $pkg | sed 's/+/\\\+/g'))#.*(t.*z)" | sort -ur))

            for raw in ${PKG_NAME[*]};do
                if [[ $DISTR == sla* ]]; then
                   [[ $(echo $raw | rev | cut -d '-' -f4- | rev | grep -ox $pkg) ]] && _PKG_NAME=$raw
                elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
                   [[ $(echo $raw | cut -d "#" -f1 | grep -ox $pkg) ]] && _PKG_NAME=${raw/\#/%23}
                fi
            done

            [[ -z ${_PKG_NAME} ]] && ( echo "empty download package ${category}/$pkg" >> $LOG 2>&1 && message "err" "details" && exit 1 )

            message "" "download" "package $category/${_PKG_NAME/\%23/#}"
            wget --no-check-certificate -c -nc -nd -np ${url}/${category}/${_PKG_NAME} -P $BUILD/$PKG/${type}/${ARCH}/${DISTR_VERSION}/${category}/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            unset _PKG_NAME
        fi
    done
}


install_pkg(){
    local type=$1
    local packages

    # read packages type
    read_packages "${type}" packages

    for pkg in ${packages}; do
        category=$(echo $pkg | cut -f1 -d "/")
        pkg=$(echo $pkg | cut -f2 -d "/")
        if [[ ! -z ${pkg} ]];then
            message "" "install" "package $category/${pkg}"
            if [[ $DISTR == sla* ]]; then
                ROOT=$SOURCE/$ROOTFS upgradepkg --install-new $BUILD/$PKG/${type}/${ARCH}/${DISTR_VERSION}/$category/${pkg}-* >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
                [[ $type == *-update ]] && local up="-u -f"
                # fixed install packages
                [[ ! -e $SOURCE/$ROOTFS/var/lib/pkg/db ]] && ( install -Dm644 /dev/null $SOURCE/$ROOTFS/var/lib/pkg/db >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1 )
                pkgadd ${up} --root $SOURCE/$ROOTFS $BUILD/$PKG/${type}/${ARCH}/${DISTR_VERSION}/$category/${pkg}#* >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
            fi
        fi
    done
}


install_kernel() {
    local type=$1

    message "" "install" "kernel ${KERNEL_VERSION} for $type"

    if [[ $DISTR == sla* && $type == core ]]; then
        ROOT=$SOURCE/$ROOTFS upgradepkg --install-new $BUILD/$PKG/kernel-{headers,firmware}-${SOCFAMILY}-${KERNEL_VERSION/-/_}*.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        ROOT=$SOURCE/$ROOTFS upgradepkg --install-new $BUILD/$PKG/kernel-${SOCFAMILY}-${KERNEL_VERSION/-/_}*.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    elif [[ $DISTR == sla* && ! $type =~ core|server && ! -z $DE ]]; then
        ROOT=$SOURCE/$ROOTFS upgradepkg --install-new $BUILD/$PKG/kernel-source-${SOCFAMILY}-${KERNEL_VERSION/-/_}*.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    elif [[ ( $DISTR == crux* || $DISTR == irradium ) && $type == core ]]; then
        tar xf $BUILD/$PKG/kernel-${SOCFAMILY}'#'${KERNEL_VERSION/-/_}*.tar.?z -C $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        tar xf $BUILD/$PKG/kernel-firmware-${SOCFAMILY}'#'${KERNEL_VERSION/-/_}*.tar.?z -C $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        #tar xf $BUILD/$PKG/kernel-source-${SOCFAMILY}'#'${KERNEL_VERSION/-/_}*.tar.?z -C $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}


install_ports(){
    local type=$1

    for port in ${PORTS_CRUX[@]}; do
        if [[ -d $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/${port} ]]; then
            rm -rf $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/${port} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        fi
        git_fetch $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/${port} $PORT_URL_CRUX/${port}.git "${DISTR_VERSION}::" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "install" "$DISTR: port: $port"
    done

    PORT_BRANCH=${DISTR_VERSION}
    git_fetch $SOURCE/${ROOTFS}/tmp/$DISTR ${PORT_URL_IRRADIUM}.git "${PORT_BRANCH}::" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    for port in ${PORTS_IRRADIUM[@]}; do
        [[ $type == core && $port == *xfce* ]] && continue
        if [[ -d $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/${port} ]]; then
            rm -rf $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/${port} >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        fi
        rsync -axHAWXS --numeric-ids $SOURCE/${ROOTFS}/tmp/$DISTR/${port} $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "install" "$DISTR: port: $port"
    done

    find $SOURCE/${ROOTFS}/${PORTS_INSTALL_SUFFIX}/ -type d -name "*.git" -exec rm -rf {} \+ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    if [[ -d $SOURCE/${ROOTFS}/tmp/$DISTR ]];then
        rm -rf $SOURCE/${ROOTFS}/tmp/$DISTR
    fi
}


setting_default_start_x() {
    local de="$1"
    sed "s#id:3#id:4#" -i $SOURCE/$ROOTFS/etc/inittab

    # starting the desktop environment
    ln -sf $SOURCE/$ROOTFS/etc/X11/xinit/xinitrc.${de} \
       -r $SOURCE/$ROOTFS/etc/X11/xinit/xinitrc

    if [[ -e $SOURCE/$ROOTFS/etc/rc.conf ]]; then
        # irradium (crux) added dbus, elogind, slim or lightdm
        sed -i 's:\(SERVICES=.*\)):\1 dbus elogind lightdm):g' "$SOURCE/$ROOTFS/etc/rc.conf"
    fi
}


setting_for_desktop() {
    if [[ $SOCFAMILY == sun* && -e $SOURCE/$ROOTFS/boot/boot.cmd ]]; then
        # adjustment for vdpau
        sed -i 's#sunxi_ve_mem_reserve=0#sunxi_ve_mem_reserve=128#' "$SOURCE/$ROOTFS/boot/boot.cmd"
        $SOURCE/$BOOT_LOADER_DIR/tools/mkimage -C none -A arm -T script -d $SOURCE/$ROOTFS/boot/boot.cmd \
        "$SOURCE/$ROOTFS/boot/boot.scr" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}


setting_bootloader_move_to_disk() {
    message "" "save" "bootloader data for move to disk"
#    [[ ${BOARD_NAME} == station_m1 ]] && EXCLUDES+=("trust.img" "uboot.img")

    for exclude in ${EXCLUDES[*]};do
        EXCLUDE+="--exclude=$exclude "
    done

    rsync -axHAWXS --numeric-ids $EXCLUDE $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/ $SOURCE/$ROOTFS/boot >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


setting_system() {
    message "" "setting" "system"
    rsync -axHAWXS --numeric-ids --chown=root:root $CWD/system/overall/$DISTR/ $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    if [[ -d $CWD/system/socs/$SOCFAMILY ]]; then
        rsync -axHAWXS --numeric-ids --chown=root:root $CWD/system/socs/$SOCFAMILY/ $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -d $CWD/system/socs/${SOCFAMILY}-${KERNEL_SOURCE} ]]; then
        rsync -axHAWXS --numeric-ids --chown=root:root $CWD/system/socs/${SOCFAMILY}-${KERNEL_SOURCE}/ $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -d $CWD/system/boards/${BOARD_NAME} ]]; then
        rsync -axHAWXS --numeric-ids --chown=root:root $CWD/system/boards/${BOARD_NAME}/ $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -d $CWD/system/boards/${BOARD_NAME}-${KERNEL_SOURCE} ]]; then
        rsync -axHAWXS --numeric-ids --chown=root:root $CWD/system/boards/${BOARD_NAME}-${KERNEL_SOURCE}/ $SOURCE/$ROOTFS/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    # setting for root
    if [[ -d $SOURCE/$ROOTFS/etc/skel ]]; then
        rsync -axHAWXS --numeric-ids --chown=root:root $SOURCE/$ROOTFS/etc/skel/ $SOURCE/$ROOTFS/root/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    if [[ $DISTR == crux* || $DISTR == irradium ]]; then
        # irradium, crux-arm added firstrun as service
        sed -i 's:\(SERVICES=.*\)):\1 firstrun):g' "$SOURCE/$ROOTFS/etc/rc.conf"
        # prohibit firmware update
        sed -i '/\# End of file/ i \\nUPGRADE            \^lib\/firmware\/\.\*$          NO\n' "$SOURCE/$ROOTFS/etc/pkgadd.conf"
    fi
}


setting_alsa() {
    [[ ! -z "$1" ]] && local ROOTFS="$1"

    # slackware family distribution
    if [[ -e "$SOURCE/$ROOTFS/etc/rc.d/rc.alsa" ]]; then
        message "" "setting" "default alsa"
        chmod 644 "$SOURCE/$ROOTFS/etc/rc.d/rc.pulseaudio" || exit 1
        chmod 755 "$SOURCE/$ROOTFS/etc/rc.d/rc.alsa" || exit 1
        mv "$SOURCE/$ROOTFS/etc/asound.conf" "$SOURCE/$ROOTFS/etc/asound.conf.new" || exit 1
    elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
        sed -i 's:\(SERVICES=.*\)\(firstrun\)\(.*\):\1\2 alsa\3:g' "$SOURCE/$ROOTFS/etc/rc.conf"
        echo -e "pcm.default { type hw card 0 }\nctl.default { type hw card 0 }" > "$SOURCE/$ROOTFS/etc/asound.conf"
    fi
}


setting_hostname() {
    message "" "setting" "hostname"
    if [[ $DISTR == sla* ]]; then
        echo $BOARD_NAME | sed 's/_/-/g' > "$SOURCE/$ROOTFS/etc/HOSTNAME"
    elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
        echo $BOARD_NAME | sed 's/_/-/g' | xargs -I {} sed -i 's:\(^HOSTNAME=\).*:\1{}:g' "$SOURCE/$ROOTFS/etc/rc.conf"
    fi
}


setting_networkmanager() {
    [[ ! -z "$1" ]] && local ROOTFS="$1"

    # slackware family distribution
    if [[ -e "$SOURCE/$ROOTFS/etc/rc.d/rc.networkmanager" ]]; then
        message "" "setting" "networkmanager"
        chmod 755 "$SOURCE/$ROOTFS/etc/rc.d/rc.networkmanager" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}


setting_ntp() {
    message "" "setting" "ntp"
    if [[ $DISTR == sla* ]]; then
        chmod 755 "$SOURCE/$ROOTFS/etc/rc.d/rc.ntpd" || exit 1
        sed 's:^#server:server:g' -i "$SOURCE/$ROOTFS/etc/ntp.conf" || exit 1
    elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
        sed 's:^#\(.*rdate.*ntp.org$\):\1:g' $SOURCE/$ROOTFS/etc/cron/daily/rdate >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}


create_initrd() {
    if [[ $MARCH == "x86_64" ]]; then
        if [[ $(echo ${DISK_PARTITIONS[0]} | grep -E '.*boot.*fat.*') ]]; then
            find "$SOURCE/$ROOTFS/boot/" -type l -exec rm -rf {} \+ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        fi
        return 0
    fi

    message "" "create" "initrd"

    KERNEL_VERSION=$(get_version $SOURCE/$KERNEL_DIR)

    mount --bind /dev "$SOURCE/$ROOTFS/dev"
    mount --bind /proc "$SOURCE/$ROOTFS/proc"

    if [[ $DISTR != crux* && $DISTR != irradium ]]; then
        # mkinitrd corrupted for ARM
        export MKINITRD_ALLOWEXEC=yes && echo "export MKINITRD_ALLOWEXEC=yes" > "$SOURCE/$ROOTFS/tmp/initrd.sh"
        echo "mkinitrd -R -L -u -w 2 -c -k ${KERNEL_VERSION} -m ${INITRD_MODULES} \\" >> "$SOURCE/$ROOTFS/tmp/initrd.sh"
        echo "         -s /tmp/initrd-tree -o /tmp/initrd.gz" >> "$SOURCE/$ROOTFS/tmp/initrd.sh"
    else
        echo "mkinitramfs -k ${KERNEL_VERSION} -m ${INITRD_MODULES}" > "$SOURCE/$ROOTFS/tmp/initrd.sh"
        echo "mv /tmp/initrd.img-${KERNEL_VERSION} /tmp/initrd-${KERNEL_VERSION}.img" >> "$SOURCE/$ROOTFS/tmp/initrd.sh"
    fi

    [[ $MARCH == "aarch64" && ( $ARCH == "riscv64" || $ARCH == "arm" ) ]] && prepare_chroot "prepare"
    chroot "$SOURCE/$ROOTFS" /bin/bash -c 'chmod +x /tmp/initrd.sh 2>&1 && /tmp/initrd.sh 2>&1 ' >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    [[ $MARCH == "aarch64" && ( $ARCH == "riscv64" || $ARCH == "arm" ) ]] && prepare_chroot "cleaning"

    umount "$SOURCE/$ROOTFS/proc"
    umount "$SOURCE/$ROOTFS/dev"

    if [[ $DISTR != crux* && $DISTR != irradium ]]; then
        pushd "$SOURCE/$ROOTFS/tmp/initrd-tree/" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        echo "initrd-${KERNEL_VERSION}" > "$SOURCE/$ROOTFS/tmp/initrd-tree/initrd-name"
        find . | cpio --quiet -H newc -o | gzip -9 -n > "$SOURCE/$ROOTFS/tmp/initrd-${KERNEL_VERSION}.img" 2>/dev/null
        popd >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    mkimage -A $KARCH -O linux -T ramdisk -C gzip  -n 'uInitrd' -d "$SOURCE/$ROOTFS/tmp/initrd-${KERNEL_VERSION}.img" "$SOURCE/$ROOTFS/boot/uInitrd-${KERNEL_VERSION}" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    rm -rf $SOURCE/$ROOTFS/tmp/initrd* >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    if [[ $(echo ${DISK_PARTITIONS[0]} | grep -E '.*boot.*fat.*') ]]; then
        cp -a "$SOURCE/$ROOTFS/boot/uInitrd-${KERNEL_VERSION}" "$SOURCE/$ROOTFS/boot/uInitrd" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        find "$SOURCE/$ROOTFS/boot/" -type l -exec rm -rf {} \+ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    else
        ln -sf "$SOURCE/$ROOTFS/boot/uInitrd-${KERNEL_VERSION}" -r "$SOURCE/$ROOTFS/boot/uInitrd" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}


setting_bootloader() {
    message "" "setting" "bootloader"
    # u-boot config
    if [[ -f $CWD/config/boot_scripts/extlinux-$SOCFAMILY.conf ]]; then
        install -Dm644 $CWD/config/boot_scripts/extlinux-$SOCFAMILY.conf "$SOURCE/$ROOTFS/boot/extlinux/extlinux.conf"
        # u-boot serial inteface config
        sed -e "s:%DISTR%:${DISTR}:g" \
            -e "s:%ROOT_DISK%:UUID=${UUID_BOOT_FS_EXT4}:g" \
            -e "s:%DEVICE_TREE_BLOB%:${DEVICE_TREE_BLOB}:g" \
            -e "s:%SERIAL_CONSOLE%:${SERIAL_CONSOLE}:g" \
            -e "s:%SERIAL_CONSOLE_SPEED%:${SERIAL_CONSOLE_SPEED}:g" \
            -i "$SOURCE/$ROOTFS/boot/extlinux/extlinux.conf"
    fi
    if [[ -f $CWD/config/boot_scripts/boot-$SOCFAMILY.cmd ]]; then
        install -Dm644 $CWD/config/boot_scripts/boot-$SOCFAMILY.cmd "$SOURCE/$ROOTFS/boot/boot.cmd"
        # u-boot serial inteface config
        sed -e "s:%SERIAL_CONSOLE%:${SERIAL_CONSOLE}:g" \
            -e "s:%SERIAL_CONSOLE_SPEED%:${SERIAL_CONSOLE_SPEED}:g" \
            -i "$SOURCE/$ROOTFS/boot/boot.cmd"
    fi
    if [[ -f $CWD/config/boot_scripts/boot-$SOCFAMILY.ini ]];then
        install -Dm644 $CWD/config/boot_scripts/boot-$SOCFAMILY.ini "$SOURCE/$ROOTFS/boot/boot.ini"
        # u-boot serial inteface config
        sed -e "s:%SERIAL_CONSOLE%:${SERIAL_CONSOLE}:g" \
            -e "s:%SERIAL_CONSOLE_SPEED%:${SERIAL_CONSOLE_SPEED}:g" \
            -i "$SOURCE/$ROOTFS/boot/boot.ini"
    fi
    # amlogic tv box: compile boot script
    if [[ -f $CWD/config/boot_scripts/boot-${BOARD_NAME//_/-}-aml_autoscript.cmd ]]; then
        install -Dm644 $CWD/config/boot_scripts/boot-${BOARD_NAME//_/-}-aml_autoscript.cmd "$SOURCE/$ROOTFS/boot/aml_autoscript.cmd" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        $SOURCE/$BOOT_LOADER_DIR/tools/mkimage -C none -A $KARCH -T script -a 0 -e 0 -d $SOURCE/$ROOTFS/boot/aml_autoscript.cmd \
                                                                        "$SOURCE/$ROOTFS/boot/aml_autoscript" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -f $CWD/config/boot_scripts/boot-${BOARD_NAME//_/-}-emmc_autoscript.cmd ]]; then
        install -Dm644 $CWD/config/boot_scripts/boot-${BOARD_NAME//_/-}-emmc_autoscript.cmd "$SOURCE/$ROOTFS/boot/emmc_autoscript.cmd" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        $SOURCE/$BOOT_LOADER_DIR/tools/mkimage -C none -A $KARCH -T script -a 0 -e 0 -d $SOURCE/$ROOTFS/boot/emmc_autoscript.cmd \
                                                                        "$SOURCE/$ROOTFS/boot/emmc_autoscript" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -f $CWD/config/boot_scripts/boot-${BOARD_NAME//_/-}-s905_autoscript.cmd ]]; then
        install -Dm644 $CWD/config/boot_scripts/boot-${BOARD_NAME//_/-}-s905_autoscript.cmd "$SOURCE/$ROOTFS/boot/s905_autoscript.cmd" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        $SOURCE/$BOOT_LOADER_DIR/tools/mkimage -C none -A $KARCH -T script -a 0 -e 0 -d $SOURCE/$ROOTFS/boot/s905_autoscript.cmd \
                                                                        "$SOURCE/$ROOTFS/boot/s905_autoscript" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    # compile boot script
    # for rk35xx, when using mkimage from the rockchip/radxa team/firefly team
    # repository, an incorrect header is generated
    [[ $SOCFAMILY != rk35* ]] && MKIMAGE_PATH="$SOURCE/$BOOT_LOADER_DIR/tools/"
    [[ -f $SOURCE/$ROOTFS/boot/boot.cmd ]] && ( ${MKIMAGE_PATH}mkimage -C none -A $KARCH -T script -d $SOURCE/$ROOTFS/boot/boot.cmd \
                                                                        "$SOURCE/$ROOTFS/boot/boot.scr" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1 )
    # u-boot
    if [[ -f "$CWD/config/boot_scripts/uEnv-$SOCFAMILY.txt" ]]; then
        install -Dm644 $CWD/config/boot_scripts/uEnv-$SOCFAMILY.txt "$SOURCE/$ROOTFS/boot/uEnv.txt"
        echo "fdtfile=${DEVICE_TREE_BLOB}" >> "$SOURCE/$ROOTFS/boot/uEnv.txt"
        # parameter to configure the boot of the legacy kernel
        [[ $SOCFAMILY == meson-sm1 && ( $KERNEL_SOURCE != next && $BOARD_NAME != x96_max_plus ) ]] && echo "kernel=$KERNEL_SOURCE" >> "$SOURCE/$ROOTFS/boot/uEnv.txt"
    fi
    # set root disk
    if [[ ! $(grep ${UUID_ROOT_FS_EXT4} "$SOURCE/$ROOTFS/boot/uEnv.txt") ]]; then
        echo "rootdev=UUID=${UUID_ROOT_FS_EXT4}" >> "$SOURCE/$ROOTFS/boot/uEnv.txt"
    fi
}


setting_governor() {
    if [[ ! -z $CPU_GOVERNOR ]]; then
        message "" "setting" "governor"
        sed "s:#SCALING_\(.*\)=\(.*\):SCALING_\1=$CPU_GOVERNOR:g" -i $SOURCE/$ROOTFS/etc/default/cpufreq
    fi
}


setting_datetime() {
    message "" "setting" "datetime"
    # setting build time
    [[ -e $SOURCE/$ROOTFS/usr/local/bin/fakehwclock.sh ]] && touch $SOURCE/$ROOTFS/usr/local/bin/fakehwclock.sh || (message "err" "details" && exit 1) || exit 1

    rm -f $SOURCE/$ROOTFS/etc/localtime* >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    ln -vfs /usr/share/zoneinfo/UTC $SOURCE/$ROOTFS/etc/localtime-copied-from  >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    cp -favv $SOURCE/$ROOTFS/usr/share/zoneinfo/UTC $SOURCE/$ROOTFS/etc/localtime >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


setting_dhcp() {
    message "" "setting" "eth0 enabled dhcp"
    # set eth0 to be DHCP by default
    if [[ $DISTR == sla* ]]; then
        sed -i 's/USE_DHCP\[0\]=.*/USE_DHCP\[0\]="yes"/g' $SOURCE/$ROOTFS/etc/rc.d/rc.inet1.conf
    elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
        echo $BOARD_NAME | sed 's/_/-/g' | xargs -I {} sed -i 's:\(^HOSTNAME=\).*:\1{}:g' "$SOURCE/$ROOTFS/etc/rc.conf"
        sed -e 's:^\(DEV=\).*:\1eth0:' -e 's:^\(DHCPOPTS=.*\)":\1 \$\{DEV\}":' -i "$SOURCE/$ROOTFS/etc/rc.d/net"
    fi
}


setting_ssh() {
    message "" "setting" "ssh login under the root"
    sed -e 's/^\(#\)\(PermitRootLogin\).*/\2 yes/g' \
        -e 's/^\(#\)\(PasswordAuth.*\)/\2/g' \
        -e 's/^\(#\)\(PermitEmptyPasswords\).*/\2 yes/g' \
        -e 's/^\(UsePAM\).*/\1 no/g' \
    -i "$SOURCE/$ROOTFS/etc/ssh/sshd_config"
    # crux-arm added sshd in service
    if [[ $DISTR == crux* || $DISTR == irradium ]]; then
        sed -i 's:\(SERVICES=.*\)\(net\)\(.*\):\1\2 sshd\3:g' "$SOURCE/$ROOTFS/etc/rc.conf"
    fi
}


setting_modules() {
    if [[ ! -z ${MODULES} ]]; then
        message "" "setting" "modules to load at system startup: ${MODULES[*]}"
        if [[ $DISTR == sla* ]]; then
            tr ' ' '\n' <<< "${MODULES}" | sed -e 's/^/\/sbin\/modprobe /' >> "$SOURCE/$ROOTFS/etc/rc.d/rc.modules.local"
            chmod 755 "$SOURCE/$ROOTFS/etc/rc.d/rc.modules.local"
        elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
            tr ' ' '\n' <<< "${MODULES}" | sed -e 's/^/\/sbin\/modprobe /' | xargs -I{} sed -i '/depmod\s-a/a {}' "$SOURCE/$ROOTFS/etc/rc.modules"
        fi
    fi
    if [[ ! -z ${MODULES_BLACKLIST} ]]; then
        message "" "setting" "blacklist modules: ${MODULES_BLACKLIST}"
        tr ' ' '\n' <<< "${MODULES_BLACKLIST}" | sed -e 's/^/blacklist /' > "$SOURCE/$ROOTFS/etc/modprobe.d/blacklist-${BOARD_NAME}.conf"
    fi
}


setting_overlays() {
    if [[ ! -z ${OVERLAYS} ]]; then
        message "" "setting" "up dtbo files: ${OVERLAYS}"
        echo "overlays=${OVERLAYS}" >> "$SOURCE/$ROOTFS/boot/uEnv.txt"
    fi
    if [[ ! -z $OVERLAY_PREFIX ]]; then
        echo "overlay_prefix=$OVERLAY_PREFIX" >> "$SOURCE/$ROOTFS/boot/uEnv.txt"
    fi
}


removed_default_xorg_conf() {
    [[ ! -z "$1" ]] && local ROOTFS="$1"

    if [[ -e $SOURCE/$ROOTFS/etc/X11/xorg.conf.d/xorg.conf ]]; then
        message "" "setting" "removed default xorg.conf"
        rm -rf "$SOURCE/$ROOTFS/etc/X11/xorg.conf.d/xorg.conf" || exit 1
    fi
}


image_compression() {
    local IMG="$1"
    pushd $BUILD/$OUTPUT/$IMAGES >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    COMPRESSOR="zstd"
    EXT="zst"
    PARAMETERS="-qf12 --rm -T${CPUS}"
    message "" "compression" "$COMPRESSOR $PARAMETERS ${IMG}.img"
    pushd $BUILD/$OUTPUT/$IMAGES >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    $COMPRESSOR $PARAMETERS ${IMG}.img
    # testing
    message "" "testing" "$COMPRESSOR -qt ${IMG}.img.${EXT}"
    $COMPRESSOR -qt ${IMG}.img.${EXT}
    # create checksum
    local CHECKSUM_EXT="sha256"
    local CHECKSUM="${CHECKSUM_EXT}sum"
    message "" "create" "checksum ${CHECKSUM} ${IMG}.img.${EXT}.${CHECKSUM_EXT}"
    ${CHECKSUM} ${IMG}.img.${EXT} > ${IMG}.img.${EXT}.${CHECKSUM_EXT}
    popd >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}

