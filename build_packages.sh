#!/bin/bash



if [ -z $CWD ]; then
    exit
fi


build_kernel_pkg() {
    cd $SOURCE
    # get kernel version
    KERNEL_VERSION=$(get_version $SOURCE/$KERNEL_DIR)

    KERNEL=zImage
    [[ $KARCH == arm64 || $KARCH == riscv ]] && KERNEL=Image

    message "" "copy" "linux-firmware"
    # create linux firmware
    [[ ! -d "$BUILD/$PKG/kernel-firmware/lib/firmware" ]] && mkdir -p $BUILD/$PKG/kernel-firmware/lib/firmware
    rsync -axHAWXS --numeric-ids --exclude .git $SOURCE/$KERNEL_FIRMWARE_DIR/ $BUILD/$PKG/kernel-firmware/lib/firmware/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    # adding custom firmware
    if [[ $FIRMWARE == "yes" ]];then
        cp -vaf $CWD/blobs/firmware/overall/* $BUILD/$PKG/kernel-firmware/lib/firmware/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        [[ -d $CWD/blobs/firmware/${BOARD_NAME} ]] && ( cp -vaf $CWD/blobs/firmware/${BOARD_NAME}/* $BUILD/$PKG/kernel-firmware/lib/firmware/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1 )
        [[ -d $CWD/blobs/firmware/${BOARD_NAME}-${KERNEL_SOURCE} ]] && ( cp -vaf $CWD/blobs/firmware/${BOARD_NAME}-${KERNEL_SOURCE}/* $BUILD/$PKG/kernel-firmware/lib/firmware/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1 )
    fi

    message "" "copy" "kernel"
    # install kernel
    install -Dm644 $SOURCE/$KERNEL_DIR/arch/${KARCH}/boot/$KERNEL $BUILD/$PKG/kernel-${SOCFAMILY}/boot/vmlinuz-${KERNEL_VERSION}
    install -Dm644 $SOURCE/$KERNEL_DIR/System.map $BUILD/$PKG/kernel-${SOCFAMILY}/boot/System.map-${KERNEL_VERSION}
    install -Dm644 $SOURCE/$KERNEL_DIR/.config $BUILD/$PKG/kernel-${SOCFAMILY}/boot/config-${KERNEL_VERSION}
    if [[ $(echo ${DISK_PARTITIONS[*]} | grep -Po '.*\d+:boot:ext4.*') || ${#DISK_PARTITIONS[*]} == 1 ]]; then
        # Make symlinks:
        ln -sf System.map-${KERNEL_VERSION} $BUILD/$PKG/kernel-${SOCFAMILY}/boot/System.map
        ln -sf config-${KERNEL_VERSION} $BUILD/$PKG/kernel-${SOCFAMILY}/boot/config
        ln -sf vmlinuz-${KERNEL_VERSION} $BUILD/$PKG/kernel-${SOCFAMILY}/boot/vmlinuz
        ln -sf vmlinuz-${KERNEL_VERSION} $BUILD/$PKG/kernel-${SOCFAMILY}/boot/$KERNEL
        message "" "setup" "device tree blobs"
        # create symlink device tree blobs
        ln -sf $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtbs/${KERNEL_VERSION} -r $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtb >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    else
        install -Dm644 $SOURCE/$KERNEL_DIR/arch/${KARCH}/boot/$KERNEL $BUILD/$PKG/kernel-${SOCFAMILY}/boot/$KERNEL
        install -Dm644 $SOURCE/$KERNEL_DIR/System.map $BUILD/$PKG/kernel-${SOCFAMILY}/boot/System.map
        install -Dm644 $SOURCE/$KERNEL_DIR/.config $BUILD/$PKG/kernel-${SOCFAMILY}/boot/config
    fi

    if [[ $SOCFAMILY == bcm2* ]]; then
        install -dm755 $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtb
        cp -av $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtbs/${KERNEL_VERSION}/broadcom/* \
                $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtb/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        cp -av $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtbs/${KERNEL_VERSION}/broadcom/* \
                $BUILD/$PKG/kernel-${SOCFAMILY}/boot/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        cp -av $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtbs/${KERNEL_VERSION}/overlays \
                $BUILD/$PKG/kernel-${SOCFAMILY}/boot/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    # when partitioning the disk, boot and fs fat copies dtbs to the root of dtb
    if [[ $(echo ${DISK_PARTITIONS[0]} | grep -E '.*boot.*fat.*') ]]; then
        install -dm755 "$BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtb"
        cp -av $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtbs/${KERNEL_VERSION}/* $BUILD/$PKG/kernel-${SOCFAMILY}/boot/dtb/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    cd "$CWD" # fix actual current directory
    # clean-up unnecessary files generated during install
    find "$BUILD/$PKG/kernel-${SOCFAMILY}" "$BUILD/$PKG/kernel-headers" \( -name .install -o -name ..install.cmd \) -delete >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    # split install_modules -> firmware
    install -dm755 "$BUILD/$PKG/kernel-firmware/lib"
    if [ -d $BUILD/$PKG/kernel-${SOCFAMILY}/lib/firmware ];then
        message "" "preparation" "of firmware"
        cp -af "$BUILD/$PKG/kernel-${SOCFAMILY}/lib/firmware" "$BUILD/$PKG/kernel-firmware/lib"
        rm -rf "$BUILD/$PKG/kernel-${SOCFAMILY}/lib/firmware"
    fi

    cd $BUILD/$PKG/kernel-${SOCFAMILY}/lib/modules/${KERNEL_VERSION}*
    if [[ -d "$BUILD/$PKG/kernel-${SOCFAMILY}/lib/modules/${KERNEL_VERSION}/build" ]]; then
        rm -rf "build" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        ln -s /usr/src/linux-${KERNEL_VERSION} build
    fi
    if [[ -d "$BUILD/$PKG/kernel-${SOCFAMILY}/lib/modules/${KERNEL_VERSION}/source" ]]; then
        rm -rf "source" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        ln -s /usr/src/linux-${KERNEL_VERSION} source
    fi

    # build kernel-source
    [[ ! -d "$BUILD/$PKG/kernel-source" ]] && mkdir -p $BUILD/$PKG/kernel-source/usr/src/linux-${KERNEL_VERSION}
    rsync -axHAWXS --numeric-ids --exclude .git --delete $SOURCE/$KERNEL_DIR/ $BUILD/$PKG/kernel-source/usr/src/linux-${KERNEL_VERSION}/
    cd $BUILD/$PKG/kernel-source/usr/src/linux-${KERNEL_VERSION}/
    make ARCH=$KARCH CROSS_COMPILE=$CROSS clean >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    # Make sure header files aren't missing...
    make ARCH=$KARCH CROSS_COMPILE=$CROSS prepare >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    # Don't package the kernel in the sources:
    find . -name "*Image" -exec rm "{}" \+
    # No need for these:
    rm -f .config.old .version
    find . -name "*.cmd" -exec rm -f "{}" \+ 
    rm .*.d
    cd ..
    ln -sf linux-${KERNEL_VERSION} linux

    if [[ $DISTR == sla* ]]; then
        message "" "create" "package of kernel"
        # create kernel package
        cd $BUILD/$PKG/kernel-${SOCFAMILY}/ && mkdir "install"
        cat "$CWD/packages/kernel/slack-desc.kernel-template" | sed "s:%SOCFAMILY%:${SOCFAMILY}:g" > "$BUILD/$PKG/kernel-${SOCFAMILY}/install/slack-desc"
        makepkg -l n -c n $BUILD/$PKG/kernel-${SOCFAMILY}-${KERNEL_VERSION/-/_}-${ARCH}-${RELEASE}${PACKAGER}.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

        message "" "create" "package kernel-headers"
        # create kernel-headers package
        cd $BUILD/$PKG/kernel-headers/ && mkdir "install"
        cat "$CWD/packages/kernel/slack-desc.kernel-headers" | sed "s:%SOCFAMILY%:${SOCFAMILY}:g" > "$BUILD/$PKG/kernel-headers/install/slack-desc"
        makepkg -l n -c n $BUILD/$PKG/kernel-headers-${SOCFAMILY}-${KERNEL_VERSION/-/_}-${ARCH}-${RELEASE}${PACKAGER}.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

        message "" "create" "package kernel-firmware"
        # create kernel-firmware package
        cd $BUILD/$PKG/kernel-firmware/ && mkdir "install"
        cat "$CWD/packages/kernel/slack-desc.kernel-firmware" | sed "s:%SOCFAMILY%:${SOCFAMILY}:g" > "$BUILD/$PKG/kernel-firmware/install/slack-desc"
        makepkg -l n -c n $BUILD/$PKG/kernel-firmware-${SOCFAMILY}-${KERNEL_VERSION/-/_}-${ARCH}-${RELEASE}${PACKAGER}.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

        message "" "create" "package kernel-source"
        # create kernel-source package
        cd $BUILD/$PKG/kernel-source/ && mkdir "install"
        cat "$CWD/packages/kernel/slack-desc.kernel-source" | sed "s:%SOCFAMILY%:${SOCFAMILY}:g" > "$BUILD/$PKG/kernel-source/install/slack-desc"
        makepkg -l n -c n $BUILD/$PKG/kernel-source-${SOCFAMILY}-${KERNEL_VERSION/-/_}-noarch-${RELEASE}${PACKAGER}.txz >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    elif [[ $DISTR == crux* || $DISTR == irradium ]]; then
        message "" "create" "package of kernel"
        # create kernel package
        cd $BUILD/$PKG/kernel-${SOCFAMILY}/
        tar czf $BUILD/$PKG/kernel-${SOCFAMILY}"#"${KERNEL_VERSION/-/_}-${RELEASE}.pkg.tar.gz . >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

        #message "" "create" "package kernel-headers"
        # create kernel-headers package
        #cd $BUILD/$PKG/kernel-headers/
        #tar czf $BUILD/$PKG/kernel-headers-${SOCFAMILY}'#'${KERNEL_VERSION/-/_}-${RELEASE}.pkg.tar.gz . >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

        message "" "create" "package kernel-firmware"
        # create kernel-firmware package
        cd $BUILD/$PKG/kernel-firmware/
        tar czf $BUILD/$PKG/kernel-firmware-${SOCFAMILY}'#'${KERNEL_VERSION/-/_}-${RELEASE}.pkg.tar.gz . >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

        message "" "create" "package kernel-source"
        # create kernel-source package
        cd $BUILD/$PKG/kernel-source/
        tar czf $BUILD/$PKG/kernel-source-${SOCFAMILY}'#'${KERNEL_VERSION/-/_}-${RELEASE}.pkg.tar.gz . >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    cd $BUILD/$PKG

    # clear kernel packages directories
    [[ -d "$BUILD/$PKG/kernel-${SOCFAMILY}" ]] && \
        rm -rf "$BUILD/$PKG/kernel-${SOCFAMILY}" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    [[ -d "$BUILD/$PKG/kernel-headers" ]] && \
        rm -rf "$BUILD/$PKG/kernel-headers" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    [[ -d "$BUILD/$PKG/kernel-firmware" ]] && \
        rm -rf "$BUILD/$PKG/kernel-firmware" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    [[ -d "$BUILD/$PKG/kernel-source" ]] && \
        rm -rf "$BUILD/$PKG/kernel-source" >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


#---------------------------------------------
# creation of the archive with the bootloader
#---------------------------------------------
create_archive_bootloader() {
    message "" "create" "archive bootloader"
    cd $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/ || exit 1
    tar cJf $BUILD/$OUTPUT/$IMAGES/boot-${ROOTFS_VERSION}.tar.xz boot || exit 1
}


