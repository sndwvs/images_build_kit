#!/bin/bash



if [ -z $CWD ]; then
    exit
fi

#---------------------------------------------
# external wifi driver configuration
#---------------------------------------------
[[ -z $EXTERNAL_WIFI ]] && EXTERNAL_WIFI=yes

#---------------------------------------------
# external wireguard driver configuration
#---------------------------------------------
[[ -z $EXTERNAL_WIREGUARD ]] && EXTERNAL_WIREGUARD=yes


#---------------------------------------------
# board configuration
#---------------------------------------------
get_config




#---------------------------------------------
# mainline kernel source configuration
#---------------------------------------------
if [[ $USE_NEXT_KERNEL_MIRROR == yes ]]; then
    LINUX_SOURCE=${LINUX_SOURCE:-"https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux-stable"}
else
    LINUX_SOURCE=${LINUX_SOURCE:-"git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"}
fi
KERNEL_BRANCH=${KERNEL_BRANCH:-"linux-$KERNEL_BRANCH_VERSION.y::"}
KERNEL_DIR=${KERNEL_DIR:-"linux-$KERNEL_SOURCE-$KERNEL_BRANCH_VERSION"}

#---------------------------------------------
# mainline kernel firmware configuration
#---------------------------------------------
KERNEL_FIRMWARE_SOURCE=${KERNEL_FIRMWARE_SOURCE:-"git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git"}
KERNEL_FIRMWARE_BRANCH=${KERNEL_FIRMWARE_BRANCH:-"main::"}
KERNEL_FIRMWARE_DIR=${KERNEL_FIRMWARE_DIR:-"linux-firmware"}

#---------------------------------------------
# configuration linux distribution
#---------------------------------------------
if [[ $DISTR == sla* ]]; then
    [[ $DISTR == slackwarearm ]] && DISTR_VERSION="15.0"
    DISTR_VERSION=${DISTR_VERSION:-"current"} # or 15.0
elif [[ $DISTR == crux* ]]; then
    DISTR_VERSION=${DISTR_VERSION:-"3.7"}
elif [[ $DISTR == irradium ]]; then
    DISTR_VERSION=${DISTR_VERSION:-"3.7"}
fi

#---------------------------------------------
# configuration build images: core  and desktop environment
#---------------------------------------------
if [[ -z $DISTR_IMAGES && ! -z $DE ]]; then
    DISTR_IMAGES=("core" "$DE")
elif [[ $DISTR_IMAGES == "core" && ! -z $DE ]]; then
    DISTR_IMAGES+=("$DE")
elif [[ ! -z $DISTR_IMAGES && $DISTR_IMAGES != "core" && -z $DE ]]; then
    DISTR_IMAGES=("core")
fi

#---------------------------------------------
# boot loader configuration
#---------------------------------------------
if [[ $USE_UBOOT_MIRROR == yes ]]; then
    BOOT_LOADER_SOURCE=${BOOT_LOADER_SOURCE:-"https://github.com/u-boot/u-boot.git"}
else
    BOOT_LOADER_SOURCE=${BOOT_LOADER_SOURCE:-"https://gitlab.denx.de/u-boot/u-boot.git"}
fi
BOOT_LOADER_DIR=${BOOT_LOADER_DIR:-"u-boot"}
BOOT_LOADER_BRANCH=${BOOT_LOADER_BRANCH:-"master:tag:v2025.01"} #"master:tag:v2024.10"

#---------------------------------------------
# arm trusted firmware configuration
#---------------------------------------------
ATF_SOURCE=${ATF_SOURCE:-"https://github.com/ARM-software/arm-trusted-firmware.git"}
ATF_DIR=${ATF_DIR:-"arm-trusted-firmware"}
ATF_BRANCH=${ATF_BRANCH:-"master::"}

#---------------------------------------------
# Open Source Supervisor Binary Interface (OpenSBI) configuration
#---------------------------------------------
OPENSBI_SOURCE=${OPENSBI_SOURCE:-"https://github.com/riscv-software-src/opensbi.git"}
OPENSBI_DIR=${OPENSBI_DIR:-"opensbi"}
OPENSBI_BRANCH=${OPENSBI_BRANCH:-"master:tag:v1.6"}




#---------------------------------------------
# xtools configuration
#---------------------------------------------
if [[ $MARCH == "x86_64" ]]; then
    # https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz
    # https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz
    BASE_URL_XTOOLS="https://developer.arm.com/-/media/Files/downloads/gnu-a"
    XTOOLS_ARM_SUFFIX="arm-none-linux-gnueabihf"
    XTOOLS_ARM64_SUFFIX="aarch64-none-linux-gnu"
    XTOOLS_PREFIX="gcc-arm"

    BASE_VERSION_XTOOLS="10.3-2021.07"
    VERSION_XTOOLS=$BASE_VERSION_XTOOLS

    XTOOLS+=("$XTOOLS_PREFIX-$VERSION_XTOOLS-${MARCH}-$XTOOLS_ARM_SUFFIX")
    XTOOLS+=("$XTOOLS_PREFIX-$VERSION_XTOOLS-${MARCH}-$XTOOLS_ARM64_SUFFIX")
    URL_XTOOLS+=("$BASE_URL_XTOOLS/$BASE_VERSION_XTOOLS/binrel")
    URL_XTOOLS+=("$BASE_URL_XTOOLS/$BASE_VERSION_XTOOLS/binrel")
elif [[ $MARCH == "aarch64" ]]; then
    # https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-aarch64-arm-none-linux-gnueabihf.tar.xz
    BASE_URL_XTOOLS="https://developer.arm.com/-/media/Files/downloads/gnu-a"
    XTOOLS_ARM_SUFFIX="arm-none-linux-gnueabihf"
    XTOOLS_PREFIX="gcc-arm"
    BASE_VERSION_XTOOLS="10.3-2021.07"
    VERSION_XTOOLS=$BASE_VERSION_XTOOLS
    XTOOLS+=("$XTOOLS_PREFIX-$VERSION_XTOOLS-${MARCH}-$XTOOLS_ARM_SUFFIX")
    URL_XTOOLS+=("$BASE_URL_XTOOLS/$BASE_VERSION_XTOOLS/binrel")

    if [[ $ARCH == "riscv64" ]]; then
        # source https://github.com/riscv-collab/riscv-gnu-toolchain.git
        # https://dl.irradium.org/slackware/tools/gcc-riscv64-12.2-2023.06-aarch64-riscv64-unknown-linux-gnu.tar.xz
        # source https://github.com/T-head-Semi/xuantie-gnu-toolchain.git
        # https://dl.irradium.org/slackware/tools/gcc-riscv64-10.2-2023.06-aarch64-riscv64-unknown-linux-gnu.tar.xz
        BASE_URL_XTOOLS="https://dl.irradium.org/slackware/tools"
        XTOOLS_RISCV64_SUFFIX="riscv64-unknown-linux-gnu"
        XTOOLS_PREFIX="gcc-riscv64"
        # spacemit: k1, starfive: jh7110, thead: th1520
        if [[ $SOCFAMILY == k1 || $SOCFAMILY == jh7110 || $SOCFAMILY == th1520 ]]; then
            BASE_VERSION_XTOOLS="12.2-2023.06"
        else
            BASE_VERSION_XTOOLS="10.2-2023.06"
        fi
        VERSION_XTOOLS=$BASE_VERSION_XTOOLS
        XTOOLS+=("$XTOOLS_PREFIX-$VERSION_XTOOLS-${MARCH}-$XTOOLS_RISCV64_SUFFIX")
        URL_XTOOLS+=("$BASE_URL_XTOOLS")
    fi
fi


#---------------------------------------------
# configuration distribution source base url
#---------------------------------------------
if [[ ${DISTR} == slackwar* ]];then
    DISTR_SOURCE=${DISTR_SOURCE:-"https://slackware.uk/slackwarearm"}
elif [[ ${DISTR} == slarm64 ]];then
    if [[ $USE_SLARM64_MIRROR == yes ]]; then
        DISTR_SOURCE=${DISTR_SOURCE:-"https://osdn.net/projects/slarm64/storage"}
    else
        DISTR_SOURCE=${DISTR_SOURCE:-"https://dl.irradium.org/slackware"}
    fi
elif [[ ${DISTR} == crux* ]];then
    DISTR_SOURCE=${DISTR_SOURCE:-"https://dl.irradium.org/crux"}
elif [[ ${DISTR} == irradium ]];then
    DISTR_SOURCE=${DISTR_SOURCE:-"https://dl.irradium.org/irradium"}
fi

#---------------------------------------------
# rootfs configuration
#---------------------------------------------
ROOTFS_NAME=${DISTR/earm/e}-${DISTR_VERSION}
ROOTFS_NAME=$(echo ${DISTR} | sed "s:arm\|aarch64::g")-${DISTR_VERSION}
ROOTFS_VERSION=$(date +%Y%m%d)

#---------------------------------------------
# cross compilation
#---------------------------------------------
if [[ $MARCH == "x86_64" || $MARCH == "aarch64" ]]; then
    for XTOOL in ${XTOOLS[*]}; do
        if [[ $MARCH == "x86_64" && $XTOOL =~ "aarch64" ]]; then
            [[ $XTOOLS_ARM64_SUFFIX =~ "aarch64" ]] && _XTOOLS_ARM_SUFFIX=$XTOOLS_ARM64_SUFFIX
            export CROSS="${SOURCE}/$XTOOL/bin/${_XTOOLS_ARM_SUFFIX}-"
        fi
        if [[ $XTOOL =~ "gnueabihf" ]]; then
            [[ $XTOOLS_ARM_SUFFIX =~ "gnueabihf" ]] && _XTOOLS_ARM_SUFFIX=$XTOOLS_ARM_SUFFIX
            export CROSS32="${SOURCE}/$XTOOL/bin/${_XTOOLS_ARM_SUFFIX}-"
        fi
        if [[ $XTOOL =~ "riscv64" ]]; then
            [[ $XTOOLS_RISCV64_SUFFIX =~ "riscv64" ]] && _XTOOLS_RISCV64_SUFFIX=$XTOOLS_RISCV64_SUFFIX
            export CROSS="${SOURCE}/$XTOOL/bin/${_XTOOLS_RISCV64_SUFFIX}-"
        fi
    done
fi

[[ $MARCH != "x86_64" && $ARCH != "riscv64" ]] && export CROSS=""
[[ $MARCH == "aarch64" && $KARCH == "arm" ]] && export CROSS=$CROSS32

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:$BUILD/$OUTPUT/$TOOLS/
#export PATH=/bin:/sbin:/usr/bin:/usr/sbin:$SOURCE/$ARM_XTOOLS/bin:$SOURCE/$ARM64_XTOOLS/bin:$BUILD/$OUTPUT/$TOOLS/
#export CROSS="${XTOOLS_ARM_SUFFIX}-"
#export CROSS64="${XTOOLS_ARM64_SUFFIX}-"

#---------------------------------------------
# packages
#---------------------------------------------
if [[ ${DISTR} == slackware* ]]; then
    DISTR_DIR=$(echo ${DISTR} | sed "s:arm\|aarch64::g")
elif [[ ${DISTR} == slarm64 ]]; then
    DISTR_DIR=${DISTR}
elif [[ ${DISTR} == crux* ]]; then
    unset DISTR_DIR
elif [[ ${DISTR} == irradium ]]; then
    unset DISTR_DIR
fi
[[ $ARCH == riscv64 ]] && DISTR_SUFFIX="-$ARCH"
DISTR_URL="${DISTR_SOURCE}/${DISTR}${DISTR_SUFFIX}-${DISTR_VERSION}/${DISTR_DIR}"
DISTR_EXTRA_URL="${DISTR_SOURCE}/packages/${ARCH}"
#DISTR_URL="https://dl.irradium.org/slackware/${DISTR}-${DISTR_VERSION}/${DISTR_DIR}"
#DISTR_EXTRA_URL="https://dl.irradium.org/slackware/packages/${ARCH}"
if [[ ${DISTR} == crux* ]]; then
    if [[ ${ARCH} == aarch64 ]]; then
        DISTR_URL="${DISTR_SOURCE}/pkg/${DISTR_VERSION}-${ARCH}"
    else
        DISTR_URL="${DISTR_SOURCE}/pkg/${DISTR_VERSION}"
    fi
elif [[ ${DISTR} == irradium ]]; then
    DISTR_URL="${DISTR_SOURCE}/packages/${ARCH}/${DISTR_VERSION}"
fi

#---------------------------------------------
# clean enviroment
#---------------------------------------------
clean_sources (){
    [[ -d $BUILD ]] && rm -rf $BUILD/ || exit 1
}

#---------------------------------------------
# create enviroment
#---------------------------------------------
prepare_dest (){
    mkdir -p {$BUILD/$OUTPUT/{$TOOLS,$IMAGES},$SOURCE} || exit 1
}

